﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;

namespace SIR_Praksa_CustomControls
{
    public class SearchTextbox : TextBox
    {
        public static DependencyProperty PlaceholderTextProperty =
            DependencyProperty.Register(
                "PlaceholderText",
                typeof(string),
                typeof(SearchTextbox));

        public static DependencyProperty PlaceholderTextColorProperty =
            DependencyProperty.Register(
                "PlaceholderTextColor",
                typeof(Brush),
                typeof(SearchTextbox));

        private static DependencyPropertyKey HasTextPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "HasText",
                typeof(bool),
                typeof(SearchTextbox),
                new PropertyMetadata());
        public static DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;        

        public static readonly RoutedEvent SearchEvent =
            EventManager.RegisterRoutedEvent(
                "Search",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(SearchTextbox));

        static SearchTextbox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(SearchTextbox),
                new FrameworkPropertyMetadata(typeof(SearchTextbox)));
        }

        public SearchTextbox()
            : base()
        {

        }

        /// <summary>
        /// Sets the HasText value based on content of TextBox
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);

            HasText = Text.Length != 0;
        }

        /// <summary>
        /// Applies the Style defined in Generic.xaml
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Border iconBorder = GetTemplateChild("PART_SearchIconBorder") as Border;
            if (iconBorder != null)
            {
                iconBorder.MouseDown += new MouseButtonEventHandler(SearchIcon_MouseDown);
            }
        }

        /// <summary>
        /// When Search Icon is pressed, Raises Search Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchIcon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            RaiseSearchEvent();
        }

        /// <summary>
        /// If Escape pressed, Erases content of TextBox.
        /// If Enter or Return pressed, Raises Search Event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Text = "";
            }
            else if ((e.Key == Key.Return || e.Key == Key.Enter))
            {
                RaiseSearchEvent();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        /// <summary>
        /// Raises Search Event
        /// </summary>
        private void RaiseSearchEvent()
        {
            if (this.Text == "")
                return;

            SearchEventArgs args = new SearchEventArgs(SearchEvent);
            args.Keyword = this.Text;

            RaiseEvent(args);
        }

        /// <summary>
        /// Placeholder text
        /// </summary>
        public string PlaceholderText
        {
            get
            {
                return (string)GetValue(PlaceholderTextProperty);
            }
            set
            {
                SetValue(PlaceholderTextProperty, value);
            }
        }

        /// <summary>
        /// Brush for Placeholder text
        /// </summary>
        public Brush PlaceholderTextColor
        {
            get
            {
                return (Brush)GetValue(PlaceholderTextColorProperty);
            }
            set
            {
                SetValue(PlaceholderTextColorProperty, value);
            }
        }

        /// <summary>
        /// True if TextBox has text. Otherwise false
        /// </summary>
        public bool HasText
        {
            get
            {
                return (bool)GetValue(HasTextProperty);
            }
            private set
            {
                SetValue(HasTextPropertyKey, value);
            }
        }

        /// <summary>
        /// Used to add a routine handling the event OnSearch in other Windows
        /// </summary>
        public event RoutedEventHandler OnSearch
        {
            add
            {
                AddHandler(SearchEvent, value);
            }
            remove
            {
                RemoveHandler(SearchEvent, value);
            }
        }
    }

    /// <summary>
    /// Catch this event in window in which you are using this Search Text Box
    /// and implement what should be searched for
    /// </summary>
    public class SearchEventArgs : RoutedEventArgs
    {
        private string keyword = "";

        /// <summary>
        /// Keyword that should be used in Search
        /// </summary>
        public string Keyword
        {
            get { return keyword; }
            set { keyword = value; }
        }

        public SearchEventArgs() : base()
        {

        }
        public SearchEventArgs(RoutedEvent routedEvent) : base(routedEvent)
        {

        }
    }
}
