﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIR_Praksa_ReusedComponents.Models
{
    public class TextModel : INotifyPropertyChanged
    {
        private string _firstText;

        public string FirstText
        {
            get
            {
                return _firstText;
            }
            set
            {
                if (_firstText != value)
                {
                    _firstText = value;
                    OnPropertyChanged(nameof(FirstText));
                }
            }
        }

        private string _secondText;

        public string SecondText
        {
            get
            {
                return _secondText;
            }
            set
            {
                if (_secondText != value)
                {
                    _secondText = value;
                    OnPropertyChanged(nameof(SecondText));
                }
            }
        }

        public TextModel(string firstText, string secondText)
        {
            FirstText = firstText;
            SecondText = secondText;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
