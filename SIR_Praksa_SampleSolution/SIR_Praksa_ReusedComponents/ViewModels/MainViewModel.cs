﻿using SIR_Praksa_ReusedComponents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SIR_Praksa_ReusedComponents.ViewModels
{
    public class MainViewModel
    {
        public TextModel TextModel { get; set; }

        public MainViewModel()
        {
            TextModel = new TextModel("Windows", "Default");
        }

        private RelayCommand _okButton;

        /// <summary>
        /// Ok Button Command
        /// </summary>
        public ICommand OKButtonCommand
        {
            get
            {
                if (_okButton == null)
                {
                    _okButton = new RelayCommand(OkButton);
                }

                return _okButton;
            }
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="sender"></param>
        private void OkButton(object sender)
        {
            MessageBox.Show("Ok button clicked");
        }

        private RelayCommand _firstAboveSeparator;

        /// <summary>
        /// First Above Separator Context Menu Command
        /// </summary>
        public ICommand FirstAboveSeparatorCommand
        {
            get
            {
                if (_firstAboveSeparator == null)
                {
                    _firstAboveSeparator = new RelayCommand(FirstAboveSeparator);
                }

                return _firstAboveSeparator;
            }
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="sender"></param>
        private void FirstAboveSeparator(object sender)
        {
            MessageBox.Show("First above separator context menu item clicked");
        }

        private RelayCommand _firstBelowSeparator;

        /// <summary>
        /// First Below Separator Context Menu Command
        /// </summary>
        public ICommand FirstBelowSeparatorCommand
        {
            get
            {
                if (_firstBelowSeparator == null)
                {
                    _firstBelowSeparator = new RelayCommand(FirstBelowSeparator);
                }

                return _firstBelowSeparator;
            }
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="sender"></param>
        private void FirstBelowSeparator(object sender)
        {
            MessageBox.Show("First below separator context menu item clicked");
        }

        private RelayCommand _secondBelowSeparator;

        /// <summary>
        /// Second Below Separator Context Menu Command
        /// </summary>
        public ICommand SecondBelowSeparatorCommand
        {
            get
            {
                if (_secondBelowSeparator == null)
                {
                    _secondBelowSeparator = new RelayCommand(SecondBelowSeparator);
                }

                return _secondBelowSeparator;
            }
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="sender"></param>
        private void SecondBelowSeparator(object sender)
        {
            MessageBox.Show("Second below separator context menu item clicked");
        }        

        private RelayCommand _cancelButton;

        /// <summary>
        /// Cancel Button Command
        /// </summary>
        public ICommand CancelButtonCommand
        {
            get
            {
                if (_cancelButton == null)
                {
                    _cancelButton = new RelayCommand(CancelButton);
                }

                return _cancelButton;
            }
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="sender"></param>
        private void CancelButton(object sender)
        {
            MessageBox.Show("Cancel button clicked");
        }

        /// <summary>
        /// Command to replace events
        /// </summary>
        public class RelayCommand : ICommand
        {
            private Action<object> execute;
            private Func<object, bool> canExecute;

            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }

            public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
            {
                this.execute = execute;
                this.canExecute = canExecute;
            }

            public bool CanExecute(object parameter)
            {
                return this.canExecute == null || this.canExecute(parameter);
            }

            public void Execute(object parameter)
            {
                this.execute(parameter);
            }
        }
    }
}
