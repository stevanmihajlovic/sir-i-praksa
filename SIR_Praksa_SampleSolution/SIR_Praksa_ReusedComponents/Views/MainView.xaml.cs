﻿using SIR_Praksa_CustomControls;
using SIR_Praksa_ReusedComponents.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SIR_Praksa_ReusedComponents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }

        private void SearchTextbox_OnSearch(object sender, RoutedEventArgs e)
        {
            SearchEventArgs args = e as SearchEventArgs;
            string keywordToSearch = args.Keyword;
            MessageBox.Show(keywordToSearch);
        }

        private void DefaultButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Default button clicked");
        }
    }
}
